import java.util.ArrayList;
import java.util.Scanner;

public class Holding {

    private String name;
    private ArrayList<Company> companys;
    private Scanner scanner;

    public Holding() {
        this.name = "Humanity Holding";
        this.companys = new ArrayList<>();
        this.scanner = new Scanner(System.in);
    }

    public Company createCompany() {
        System.out.println("Name:");
        String name = scanner.nextLine();
        System.out.println("Company No:");
        int companyNo = scanner.nextInt();
        scanner.nextLine();                 // Run sırasında satır atlama problemı ıcın.
        System.out.println("City:");
        String city = scanner.nextLine();
        System.out.println("Service:");
        String service = scanner.nextLine();
        System.out.println("Target (Profit or Non-Profit) :");
        String aim = scanner.nextLine();

        Company newCompany;
        if (aim.equals("Non-Profit")) {
            newCompany = new NonProfitCompany(companyNo, name, city, service, aim);
        } else {
            newCompany = new ProfitCompany(companyNo, name, city, service, aim);
        }
        return newCompany;
    }

    public void addCompany() {
        Company newCompany = createCompany();
        int count = 0;
        for (Company company : this.companys) {
            if (company.companyNo == newCompany.companyNo) {
                count++;
                break;
            }
        }
        if (count == 0) {
            this.companys.add(newCompany);
            System.out.println("Sirket basarıyla eklendi");
        } else {
            System.out.println("Sirket zaten mevcut.!");
        }
    }

    public void listCompany() {
        System.out.println("Holding Name: " + this.name + "\n");
        if (this.companys.isEmpty()) {
            System.out.println("Holdingde henuz bir sirket bulunmamaktadir.");
        } else {
            int no = 1;
            for (Company company : this.companys) {
                System.out.println(no + " - " + company.name + ":");
                System.out.println(company);
                System.out.println();
                no++;
            }
        }
    }

    public void removeCompany() {
        System.out.println("Silmek istediginiz sirketin kendisine ait ozel numarasini giriniz: ");
        int companyNo = scanner.nextInt();

        int flag = 0;
        for (Company company : companys) {
            if (company.companyNo == companyNo) {
                flag++;
                break;
            }
        }

        if (flag != 0) {
            for (Company company : companys) {
                if (company.companyNo == companyNo) {
                    companys.remove(company);
                    System.out.println("Silme islemi basarıyla tamamlandı.");
                    break;
                }
            }
        } else {
            System.out.println("Giris yapılan sırket numarasına aıt sırket bulunmamaktadır.");
        }
    }
}



