public class ProfitCompany extends Company implements ITaxPayment{


    public ProfitCompany(int companyNo, String name, String city, String service,String aim) {
        super(companyNo, name, city, service,aim);
    }

    @Override
    public int giveTaxt() {
        return super.tax=20;
    }

    @Override
    public String toString() {
        return  "Name: " + this.name +
                "\nCompany No: " + this.companyNo +
                "\nCity: " + this.city +
                "\nService: " + this.service +
                "\nAim: " + this.aim +
                "\nTax: " + this.giveTaxt();
    }
}
